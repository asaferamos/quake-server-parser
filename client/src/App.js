import React, { Component } from 'react';
import axios from 'axios';

import { DropdownButton, Dropdown, Table, Container, Row } from 'react-bootstrap';
import './App.css';

class LinesTable extends React.Component {
  createLines = () => {
    let table = []
    try{
      let playersCount = this.props.data.players.length
      for (let index = 0; index < playersCount; index++) {
        table.push(
          <tr key={index}>
            <td>{this.props.data.players[index]}</td>
            <td>{this.props.data.kills[this.props.data.players[index]]}</td>
          </tr>
        )
      }
    }catch(e){

    }

    table.push(
      <tr key={1000}>
        <td colSpan="2">Total Kills (with world): {this.props.data.total_kills}</td>
      </tr>
    )

    return table
  }
  render() {
    try{    
      return (
        <tbody>
          {this.createLines()}
        </tbody>
      )
    }catch(e){
      return(
        <tr>

        </tr>
      )
    }
  }
}

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
        games   : [],
        actualGame: "Select Game",
        theGame : []
    };
}

  componentDidMount() {
    axios.get(`http://localhost:3000/games`)
    .then(res => {
        this.setState({ games: res.data, theGame: res.data[0].game_1});
        console.log(res.data)
      }).catch(err => {
          console.log(err)
        }
      )
  }

  selectGame(e,i){
    this.setState({actualGame: e})

    let games = this.state.games
    games.forEach((game,i) =>{
      if(i == e)
        this.setState({theGame: game['game_'+(e+1)]})
    })

  }

  
  render() {
    return (
      <div className="App">
      <Container>
        <header className="App-header">
          <p>
            Quake Log Parser
          </p>
          <DropdownButton id="dropdown-basic-button" title={this.state.actualGame != 'Select Game' ? ('Game ' + (this.state.actualGame+1)) : 'Select Game' }>
            {
              this.state.games.map((game,i) => {
                return(
                  <Dropdown.Item key={i} onClick={this.selectGame.bind(this,i)}>Game {i+1}</Dropdown.Item>
                )
              }) 
            }
          </DropdownButton>
        </header>
        <article>
        <Table striped bordered hover variant="dark">
          <thead>
            <tr>
              <th>Players</th>
              <th>Kills</th>
            </tr>
          </thead>
          <LinesTable data={this.state.theGame} />
        </Table>
        </article>
        </Container>
      </div>
    );
  }
}

export default App;
