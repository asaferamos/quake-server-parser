# Quake Log Api Server
Esta API Server faz o parsa do arquivo de log de jogo Quake III.
Com Back-end desenvolvido em Node.js e Front-end em React.js, o código pode ser testado via Docker.

### Developers
As regras de parsas estão em `./api/src/parser/regexpRules.js`

Endpoints:
|  |  |
|--|--|
| GET | `/games` |
| GET | `/games/:id` |
| GET | `/reports` |


O endpoint `/reports` é um plus que foi pensado para ser implementado dados de relatórios estatísticos. A princípio retorna por player, o total de jogos, o total de kills, a média de kills por jogo e o desvio médio. Este último é importante para saber o quão estável é a quantidade de kills de um player. Mesmo que com menos kills por jogo, o player que possuí o menor desvio, tem mais chances de manter este número.

O app possui um client desenvolvido em React, onde é possível listar os jogos, escolher um jogo e exibir seus dados.

### Docker

> docker-compose build
> docker-compose up

Testes:
> docker exec -it quake_server npm test

Acesso:

> API: localhost:3000
> Cliente: localhost:3001


