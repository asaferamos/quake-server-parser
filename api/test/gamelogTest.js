let app = require('../app');
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

describe('Games', () => {

    describe('GET /games', () => {
        it('test for get two games from log test', (done) => {
            chai.request(app)
                .get('/games')
                .end((err, res) => {
                    res.should.have.status(200);
                    Object.keys(res.body).length.should.equal(2)                    
                  done();
                });
        });
    });

    describe('GET /games/:id', () => {
        describe('test the kill counts', () => {
            it('get a one game and test kill counts', (done) => {
                chai.request(app)
                    .get('/games/1')
                    .end((err,res) => {
                        res.should.have.status(200);
                        res.body.should.have.nested.property('game_1.total_kills',0);
                        done();
                    })
            });

            it('get the second game and test kill counts', (done) => {
                chai.request(app)
                    .get('/games/2')
                    .end((err,res) => {
                        res.should.have.status(200);
                        res.body.should.have.nested.property('game_2.total_kills',11);
                        done();
                    })
            });
        });
    });
});

describe('Games properties', () => {

    describe('GET /games/:id', () => {
        it('get the second game and test kills of the player', (done) => {
            chai.request(app)
                .get('/games/2')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.nested.property('game_2.kills.Isgalamido',-5)
                  done();
                });
        });

        it('get the second game and test players', (done) => {
            chai.request(app)
                .get('/games/2')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.nested.property('game_2.players[0]','Isgalamido')
                  done();
                });
        });
    });
});