var express = require('express');
var router = express.Router();

const parserController = require('../src/parser/parserController')
const reportsController = require('../src/reports/reportsController')

router.get('/games', (req, res) => {
	parserController.getGame().then(
		response => res.status(200).send(response)
	).catch(
		err => res.status(err.statusCode || 500).send(err)
	)
});

router.get('/games/:id', (req, res) => {
	parserController.getGame(req.params.id).then(
		response => res.status(200).send(response)
	).catch(
		err => res.status(err.statusCode || 500).send(err)
	)
});

router.get('/reports', (req, res) => {
	reportsController.getReports().then(
		response => res.status(200).send(response)
	).catch(
		err => res.status(err.statusCode || 500).send(err)
	)
});

module.exports = router;
