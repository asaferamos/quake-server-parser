module.exports = {
    getGame: (id = null) => {
        return new Promise((resolve, reject) => {
            var fs = require('fs');
            fs.readFile(`${__dirname}/../../${process.env.GAMELOG}`, 'utf8', (err, data) => {
                if (err) {
                    return reject(err)
                }

                parser = new Parser(data)
                resolve(
                    parser.getGame(id)
                )
            })
        })
    }
};

const Parser = function(data){
    const regexp = require('./regexRules')

    return{
        getGame: (id = null) => {           
            let dataLines = data.split('\n')
            
            let activeGame = 0
            let games = new Array
            dataLines.forEach((line,i) => {

                let nextLine = dataLines[i+1]
                try{
                    // test if the next line is not a new game
                    if(!nextLine.match(regexp.game) && !nextLine == ""){
                        // start game
                        if(nextLine == ' ')
                            return

                        if(line.match(regexp.game)){
                            activeGame++
                            games[activeGame] = {
                                ['game_' + activeGame]: {
                                    'total_kills': 0,
                                    'players': [],
                                    'kills': {}
                                }
                            }
                            return
                        }
                    }

                    // test if get all games or one game
                    if(!line.match(regexp.game)){
                        let lineGame = new Parser(line)
                        if(killLine = lineGame.getKills()){
                            games[activeGame]['game_'+activeGame].total_kills++

                            if(killLine.killer != '<world>'){
                                // increment kills to killer or add new killer on json
                                games[activeGame]['game_'+activeGame]
                                    .kills[killLine.killer] = (
                                        games[activeGame]['game_'+activeGame]
                                            .kills[killLine.killer] + 1 ) || 1
                            }else{
                                // decrement kills to killer or add new killer on json with -1
                                games[activeGame]['game_'+activeGame]
                                    .kills[killLine.killed] = (
                                        games[activeGame]['game_'+activeGame]
                                            .kills[killLine.killed] - 1 ) || -1
                            }
                        }

                        if(playerGame = lineGame.getPlayer()){
                            games[activeGame]['game_'+activeGame]
                                    .kills[playerGame] = 
                                        games[activeGame]['game_'+activeGame]
                                            .kills[playerGame] || 0
                        }
                    }

                    games[activeGame]['game_'+activeGame].players = 
                        Object.keys(games[activeGame]['game_'+activeGame].kills)

                }catch(e){
                    return JSON.stringify(e)
                }
            });

            if(id == null){
                // remove the first because is null
                games.shift()
                return games
            }

            return games[id]
        },
        getKills: () => {
            try{
                if(kill = data.match(regexp.kill)){
                    return {
                        killed: kill[0].match(regexp.playerKilled)[0],
                        killer: kill[0].match(regexp.playerKiller)[0]
                    }
                }else{
                    return 
                }
            }catch(e){
                return JSON.stringify(e)
            }
        },
        getPlayer: () => {
            try{
                if(player = data.match(regexp.playerGame)){
                    player = player[0].split("\\")
                    return player[1]
                }
            }catch(e){
                return JSON.stringify(e)
            }
        }
    }
}