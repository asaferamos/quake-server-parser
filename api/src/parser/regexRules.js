module.exports = {
    game:   new RegExp('--------+'),
    kill:   new RegExp(/:\s([^:]+)\skilled\s(.*?)\sby\s[a-zA-Z_]+/gm),
    playerKilled: new RegExp(/(?<=killed\s)(.*?)(?=\sby)/gm),
    playerKiller: new RegExp(/(?<=:\s)(.*?)(?=\skilled)/gm),
    playerGame  : new RegExp(/ClientUserinfoChanged: \d n\\(.*?)\\/gm)
}