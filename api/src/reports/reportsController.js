const parserController = require('../parser/parserController')

module.exports = {
    getReports: () => {
        return new Promise((resolve, reject) => {

            parserController.getGame().then(
                res => {
                    let killers = {}
                    res.forEach(game => {
                        gameId = Object.keys(game)[0]

                        let players = game[gameId].players

                        // calc average kills
                        players.forEach((playersOfGame) => {
                            try{
                                killers[playersOfGame] = {
                                    'games': killers[playersOfGame].games + 1,
                                    'kills': killers[playersOfGame].kills
                                        + game[gameId].kills[playersOfGame],
                                    'average':
                                        parseFloat(
                                            (
                                                (killers[playersOfGame].kills + game[gameId].kills[playersOfGame])
                                                / (killers[playersOfGame].games + 1)
                                            ).toFixed(4)
                                        )
                                }
                                
                            }catch(e){
                                if(e.name == 'TypeError'){
                                    killers[playersOfGame] = {
                                        'games': 1,
                                        'kills': 0,
                                        'average': 0
                                    }
                                }
                            }
  
                        })
                    });

                    calc = new Calcs()
                    killers = calc.averageDeviation(killers, res)

                    resolve(killers)
                }
            ).catch(
                err => reject(err)
            )
        })
    }
};

const Calcs = function(){
    return{
        averageDeviation: (killers, games) => {
            try{
                Object.keys(killers).forEach(player => {
                    let deviation = killers[player].average

                    // calc average deviation for players
                    games.forEach(game => {
                        gameId = Object.keys(game)[0]

                        if(game[gameId].kills[player]){
                            let deviationTemp = Math.abs(
                                deviation - game[gameId].kills[player]
                            )

                            killers[player].averageDeviation =
                                killers[player].averageDeviation + deviationTemp || deviationTemp
                        }
                    })
                })
            }catch(e){
                console.log(e)
            }

            Object.keys(killers).forEach(player => {
                killers[player].averageDeviation =
                    parseFloat(
                        (killers[player].averageDeviation / killers[player].games).toFixed(4)
                    )
            })

            return killers
        }
    }
}